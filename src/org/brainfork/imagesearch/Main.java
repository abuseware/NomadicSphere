/*
 * Copyright (c) 2020, Artur "Licho" Kaleta
 * All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 *
 * 1. Redistributions of source code must retain the above copyright notice, this
 *    list of conditions and the following disclaimer.
 * 2. Redistributions in binary form must reproduce the above copyright notice,
 *    this list of conditions and the following disclaimer in the documentation
 *    and/or other materials provided with the distribution.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND
 * ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
 * WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
 * DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT OWNER OR CONTRIBUTORS BE LIABLE FOR
 * ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
 * (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
 * LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND
 * ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
 * (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
 * SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 */

package org.brainfork.imagesearch;

import java.io.*;
import java.text.ParseException;
import java.util.Arrays;
import java.util.List;
import java.util.NoSuchElementException;
import java.util.ResourceBundle;

/**
 * Main class for console version
 */
public class Main {
    private static final String DATA_LOCATION = "data";
    private static final ResourceBundle locale = ResourceBundle.getBundle("org/brainfork/imagesearch/Locale");
    private static final PrintWriter stdout = new PrintWriter(System.out);
    private static final PrintWriter stderr = new PrintWriter(System.err);
    private static final BufferedReader stdin = new BufferedReader(new InputStreamReader(System.in));

    private static Database database = new Database();

    /**
     * Load database and populate internal structures.
     */
    private static void loadDatabase() {
        File folder = new File(DATA_LOCATION);
        stderr.println(String.format(locale.getString("InfoBrowsingDirectory"), folder.getAbsolutePath()));
        File[] files = folder.listFiles();

        if (files != null) {
            Arrays.sort(files);

            for (File file : files) {
                if (file.isFile() && file.canRead() && file.getName().endsWith(".txt")) {
                    stderr.println(String.format(locale.getString("InfoLoadingFile"), file.getName()));
                    try {
                        database.parseFile(file);
                    } catch (FileNotFoundException Exception) {
                        stderr.println(String.format(locale.getString("ErrorOpenFile"), file.getName()));
                        System.exit(1);
                    } catch (IOException Exception) {
                        stderr.println(String.format(locale.getString("ErrorReadFile"), file.getName()));
                        System.exit(1);
                    } catch (ParseException Exception) {
                        stderr.println(String.format(locale.getString("ErrorParseFile"), file.getName()));
                        System.exit(1);
                    }
                }
            }
        }
    }

    /**
     * Clear internal structures of database
     */
    private static void clearDatabase() {
        database = new Database();
    }

    /**
     * Ask user and return answer.
     *
     * @param question Have a good weekend?
     * @return Yes, thanks.
     */
    private static String askUser(String question) throws IOException {
        stdout.print(question + ": ");
        stdout.flush();
        return stdin.readLine();
    }

    /**
     * Console main()
     *
     * @param args commandline arguments
     */
    public static void main(String[] args) {

        loadDatabase();

        if (database.size() == 0) {
            stderr.println(locale.getString("ErrorNoFiles"));
            System.exit(1);
        }

        stderr.println(String.format(locale.getString("InfoLoadedRecords"), database.size()));

        char[] topLineChars = new char[79];
        Arrays.fill(topLineChars, '\u2501');
        char[] midLineChars = topLineChars.clone();
        char[] botLineChars = topLineChars.clone();

        topLineChars[0] = '\u250F';  // ┏
        topLineChars[20] = '\u2533'; // ┳
        topLineChars[33] = '\u2533'; // ┳
        topLineChars[78] = '\u2513'; // ┓

        midLineChars[0] = '\u2523';  // ┣
        midLineChars[20] = '\u254B'; // ╋
        midLineChars[33] = '\u254B'; // ╋
        midLineChars[78] = '\u252B'; // ┫

        botLineChars[0] = '\u2517';  // ┗
        botLineChars[20] = '\u253B'; // ┻
        botLineChars[33] = '\u253B'; // ┻
        botLineChars[78] = '\u251B'; // ┛

        long deltaTime;

        stdout.println();

        while (true) {
            try {
                //Asks user for (partial) SWM, quits on "!q", reloads DB on "!r"
                String query = null;
                try {
                    query = askUser(locale.getString("QuestionSWM"));
                } catch (IOException e) {
                    System.exit(1);
                }
                if (query == null) {
                    System.exit(1);
                }
                if (query.isEmpty()) {
                    stdout.println(locale.getString("ErrorNoSWM"));
                    continue;
                } else if (query.toLowerCase().equals("!q")) {
                    break;
                } else if (query.toLowerCase().equals("!r")) {
                    clearDatabase();
                    loadDatabase();
                    continue;
                }

                deltaTime = System.currentTimeMillis();
                List<Image> results = database.findImage(query);
                deltaTime = System.currentTimeMillis() - deltaTime;


                //Draw table
                stdout.println(topLineChars);
                stdout.println(
                        String.format(
                                "\u2503 %-17s \u2503 %-10s \u2503 %-42s \u2503", "SWM",
                                locale.getString("ResultSource"),
                                locale.getString("ResultInfo")
                        )
                );

                String lastSWM = null;
                for (Image result : results) {
                    if (!result.getSwm().equals(lastSWM)) {
                        stdout.println(midLineChars);
                    }

                    stdout.println(
                            String.format(
                                    "\u2503 %17.17s \u2503 %10.9s \u2503 %-42.42s \u2503",
                                    result.getSwm().equals(lastSWM) ? "" : result.getSwm(),
                                    result.getLocation(),
                                    result.getNotes() == null ? "" : result.getNotes()
                            )
                    );
                    lastSWM = result.getSwm();
                }

                stdout.println(botLineChars);
                stdout.println(locale.getString("QueryTime") + ": " + deltaTime + "ms.");
                stdout.println();

            } catch (NoSuchElementException | IllegalStateException Exception) {
                break;
            }
        }
        System.exit(0);
    }
}
