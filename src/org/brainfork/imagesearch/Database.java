/*
 * Copyright (c) 2020, Artur "Licho" Kaleta
 * All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 *
 * 1. Redistributions of source code must retain the above copyright notice, this
 *    list of conditions and the following disclaimer.
 * 2. Redistributions in binary form must reproduce the above copyright notice,
 *    this list of conditions and the following disclaimer in the documentation
 *    and/or other materials provided with the distribution.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND
 * ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
 * WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
 * DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT OWNER OR CONTRIBUTORS BE LIABLE FOR
 * ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
 * (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
 * LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND
 * ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
 * (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
 * SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 */

package org.brainfork.imagesearch;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileReader;
import java.io.IOException;
import java.text.ParseException;
import java.util.ArrayList;
import java.util.Comparator;
import java.util.List;
import java.util.stream.Collectors;

/**
 * Database of OS images (preloads) available on specific machine.
 */
class Database {
    private final ArrayList<Image> imageList = new ArrayList<>();

    /**
     * Default constructor for empty DB
     */
    Database() {
    }

    /**
     * Loads text database and fulfills internal structures with parsed data
     *
     * @param file text file from image list dump
     * @throws IOException    when file is not accessible
     * @throws ParseException when input format doesn't match requirements
     */
    Database(File file) throws IOException, ParseException {
        parseFile(file);
    }

    /**
     * Parses text from input variable to Image container class
     *
     * @param location source of images
     * @param line     input text in format SWM_INFO-1_INFO-2_(...)
     * @return Image container class with filled properties
     * @throws ParseException when input text doesn't match required format
     */
    private Image parseLine(String location, String line) throws ParseException {
        Image output = new Image();

        String[] split = line.split("_", 2);
        if (split.length < 1) {
            throw new ParseException("Wrong formatting", 0);
        }
        output.setSwm(split[0]);
        if (split.length == 2) {
            output.setNotes(split[1].replace('-', ' ').replace("_", ", "));
        }
        output.setLocation(location);
        return output;
    }

    /**
     * Loads text database and appends parsed data to internal structures
     *
     * @param file text file from image list dump
     * @throws IOException    when file is not accessible
     * @throws ParseException when input format doesn't match requirements
     */
    void parseFile(File file) throws IOException, ParseException {
        String name = file.getName().replaceFirst("[.][^.]+$", "");
        BufferedReader input = new BufferedReader(new FileReader(file));
        for (String line = input.readLine(); line != null; line = input.readLine()) {
            imageList.add(parseLine(name, line));
        }
        input.close();
    }

    /**
     * Search matching images in database
     *
     * @param swm partial SWM string
     * @return list of Image container classes
     */
    List<Image> findImage(String swm) {
        return imageList.parallelStream()
                .filter((t) -> t.getSwm().toLowerCase().contains(swm.toLowerCase()))
                .sorted(Comparator.comparing(Image::getSwm))
                .collect(Collectors.toList()
                );
    }

    /**
     * Get entry count
     *
     * @return entry count
     */
    int size() {
        return imageList.size();
    }

}
